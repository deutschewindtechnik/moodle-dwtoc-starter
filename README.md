# DWTOC Moodle Starter

Dieses Repository dient dazu, eine neue Moodle DWTOC Installation zu kopieren. Dafür wird eine Unix Konsole (z.B. git bash) benötigt.
 
```bash
$ git clone git@bitbucket.org:deutschewindtechnik/moodle-dwtoc-starter.git moodle
$ cd moodle
$ ./git.sh
```