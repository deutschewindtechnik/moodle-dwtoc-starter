rm -rf .git
git init
git remote add origin git@bitbucket.org:deutschewindtechnik/moodle.git
git pull origin master
git clone git@bitbucket.org:deutschewindtechnik/dwt_info.git local/dwt_info
git clone git@bitbucket.org:deutschewindtechnik/plugin-skills.git local/skills
git clone git@bitbucket.org:deutschewindtechnik/plugin-switch.git local/switch
git clone git@bitbucket.org:deutschewindtechnik/plugin-winda.git local/winda
git clone git@bitbucket.org:deutschewindtechnik/plugin-wmw.git local/wmw
rm -rf git.sh